api: 2
core: 8.x
projects:
  admin_toolbar: {subdir: contrib, type: module, version: '1.24'}
  config_update: {subdir: contrib, type: module, version: '1.5'}
  contact_formatter: {subdir: contrib, type: module, version: '1.0'}
  ctools: {subdir: contrib, type: module, version: '3.0'}
  drupal: {type: core}
  entity_reference_revisions: {subdir: contrib, type: module, version: '1.5'}
  features: {subdir: contrib, type: module, version: '3.7'}
  google_tag: {custom_download: true, subdir: contrib, type: module}
  link_attributes: {subdir: contrib, type: module, version: '1.3'}
  linkit: {subdir: contrib, type: module, version: '4.3'}
  menu_link_attributes: {subdir: contrib, type: module, version: '1.0'}
  metatag: {subdir: contrib, type: module, version: '1.5'}
  paragraphs: {subdir: contrib, type: module, version: '1.3'}
  pathauto: {subdir: contrib, type: module, version: '1.2'}
  redirect: {subdir: contrib, type: module, version: '1.2'}
  simple_sitemap: {subdir: contrib, type: module, version: '2.12'}
  svg_image: {subdir: contrib, type: module, version: '1.8'}
  token: {subdir: contrib, type: module, version: '1.3'}
  tvi: {subdir: contrib, type: module, version: 1.0-beta2}
